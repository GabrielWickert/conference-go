from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    data = response.json()
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return data["photos"][0]["src"]["original"]

def get_weather_data(city, state):
    geocoding_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    geocoding_response = requests.get(geocoding_url)
    geocoding_data = geocoding_response.json()
    lat = geocoding_data[0]['lat']
    lon = geocoding_data[0]['lon']
    # Create the URL for the OpenWeatherMap API with the city and state
    weather_data_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # Make the request
    weather_response = requests.get(weather_data_url)
    # Parse the JSON response
    weather_data = weather_response.json()
    # Get the main temperature and the weather's description and put them in a dictionary
    weather_dict = {
        "temperature": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    # Return the dictionary
    return weather_dict
